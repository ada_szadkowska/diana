
var myApp = angular.module('MyApp', ['ngMaterial', 'ngRoute']);

myApp.config(function($routeProvider) {
    $routeProvider

        .when('/', {
            templateUrl : 'html/home.html',
            controller  : 'mainController'
        })

        .when('/portfolio', {
            templateUrl : 'html/home.html',
            controller  : 'mainController'
        })

        .when('/about', {
            templateUrl : 'html/about.html',
            controller  : 'aboutController'
        })

        .when('/portfolio/:projectId', {
            templateUrl : 'html/portfolio.html',
            controller  : 'portfolioController'
        })

        .when('/contact', {
            templateUrl : 'html/contact.html',
            controller  : 'contactController'
        });
});

myApp.controller('AppCtrl', ['$scope', '$mdSidenav', '$location', function($scope, $mdSidenav, $location) {

    $scope.getLocation = function () {
        return $location.url()
    };

    $scope.toggleList = function() {
        $mdSidenav('left').toggle();
    };

    $scope.navigateTo = function (url) {
        $mdSidenav('left').close();
        $location.path("/" + url);
    };

    $scope.projects = [
        {
            title: "Hotel Łódź Karolew",
            id: 'hotel_lodz_karolew',
        },{
            title: "Jednostka Mieszkalna Minimum",
            id: 'jednostka_mieszkalna_minimum',
        },{
            title: "Pokój dziecka",
            id: 'pokoj_dziecka',
        }
    ];

}]);

myApp.controller('mainController', ['$scope', function($scope) {
    //$routeProvider
}]);

myApp.controller('aboutController', ['$scope', function($scope) {
    //$routeProvider
}]);

myApp.controller('portfolioController', ['$scope', '$route', function($scope, $route) {

    var params = $route.current.params;

    $scope.getProject = function () {
        angular.forEach($scope.$parent.projects, function (value, key) {
            if (value.id == params.projectId) {
                $scope.project = value;
            }
        })
    };

    $scope.getProject();
}]);

myApp.controller('contactController', function($scope) {

    // $scope.message = 'Contact us! JK. This is just a demo.';

});